const SymbolModel = require('../model/Symbol')
const CoinModel = require('../model/Coin')
// Require taapi: npm i taapi --save
const Taapi = require("taapi");
const axios = require('axios')
// Setup client with authentication


module.exports = {

    /*
    |------------------------------------------------------------------------
    |  Get All Coins Functions
    |------------------------------------------------------------------------
    */
    async get_all_coins(req, res) {

        const limit = req.query.limit || 10
        const page = req.query.page || 1

        // Get Filter
        // ---------------------------------------
        let filter = {};
        const symbol = req.query.symbol || ''

        if (symbol) {
            filter.symbol = symbol
        }

        let skip = 0
        let total_page = 0

        if ((page - 1) > 0) {
            skip = (page - 1) * limit
        }

        const resdata = await CoinModel.find(filter).skip(skip).limit(limit);
        const total_data = await CoinModel.find(filter).count();

        if (total_data > 0) {
            total_page = total_data / limit;
        }

        res.send({ status: 200, data: resdata, total: total_data, limit, page, total_page, skip });
    },

    /*
    |------------------------------------------------------------------------
    |  Sync Coin Function
    |------------------------------------------------------------------------
    */
    async sync_coin(req, res) {
        const taapi_secret = process.env.TAAPI_SECRET || "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbHVlIjoiNjNmNWRjYTJlYzkzMzI0NGEwZjI4MGQ5IiwiaWF0IjoxNjgyNzg3NjY0LCJleHAiOjMzMTg3MjUxNjY0fQ.FQ5FBzctPkBLwcCjiyrpvBJY9HY8OdJHJmeekp4NjF4"
        const max_request_bulk = 10
        // const taapi = new Taapi.default(taapi_secret);

        // Init bulk queries. This resets all previously added queries
        // ---------------------------------------
        // taapi.resetBulkConstructs();


        // Get Data Symbol
        // ---------------------------------------
        const data_symbol = await SymbolModel.find({});

        // Populate data bulkdata
        // ---------------------------------------
        const bulk_data = [];
        const exchange = process.env.TAAPI_EXCHANGE || "binance"

        // taapi.setDefaultExchange(exchange);

        const indicator = ["ma", "ema"]
        const new_data_coins = [];

        // =======================================
        // New Method
        // =======================================
        if (Array.isArray(data_symbol) && data_symbol.length > 0) {
            let params_construct = [];
            data_symbol.map((dc, index) => {
                if (max_request_bulk >= (index + 1)) {
                    params_construct.push({
                        exchange: "binance",
                        symbol: dc.coin,
                        interval: "5m",
                        indicators: [
                            ...indicator.map((e) => {
                                return e === "hma"
                                    ? {
                                        // id: `${dc.coin}-${e}`,
                                        indicator: e,
                                        period: 2,
                                    }
                                    : {
                                        // id: `${dc.coin}-${e}`,
                                        indicator: e,
                                    }
                            })
                        ]
                    })
                }
            })

            let errorResponse = '';

            await axios
                .post("https://api.taapi.io/bulk", {
                    secret: taapi_secret,
                    construct: params_construct
                })
                // Parsing Data Success
                // ---------------------------------------
                .then((response) => {

                    // New Data
                    // ---------------------------------------
                    const datas = response.data.data.map((e) => {
                        return {
                            coin: e.id.split("-")[0],
                            indicator: e.id.split("-")[1],
                            value: e.result.value
                                ? e.result.value
                                : e.result[`value${e.id.split("-")[1].toUpperCase()}`],
                        }
                    })

                    res.send({ status: 200, data: datas, message: 'Berhasil Mengambil Data Coin' });

                    data_symbol.map(async (e, i) => {
                        // GET ONE COINS
                        const c = await axios.post(
                            "https://api.tambahaset.com/coins/v1/getCoins",
                            {
                                coins: e.coin,
                            }
                        )

                        let tempData = {}

                        // LOOP AND UPDATE DATA
                        Object.keys(c.data).map((k) => {
                            if (k === "coin") {
                                tempData[k + "s"] = c.data[k]
                            }
                            let indicator = k.substring(
                                0,
                                k.indexOf("1") < 0 ? k.indexOf("2") : k.indexOf("1")
                            )

                            if (indicator) {
                                let valueAttribute = datas.filter(
                                    (data) => data.indicator === indicator && data.coin === e.coin
                                )[0].value
                                // console.log(valueAttribute)
                                let newValueAttribute = valueAttribute
                                    ? Number(
                                        valueAttribute.toString().split(".")[1].length > 8
                                            ? (valueAttribute =
                                                valueAttribute.toString().split(".")[0] +
                                                "." +
                                                valueAttribute
                                                    .toString()
                                                    .split(".")[1]
                                                    .substr(0, 8))
                                            : valueAttribute
                                    )
                                    : valueAttribute
                                if (!c.data[k]) {
                                    tempData[k] = `${newValueAttribute}/1/1`
                                } else {
                                    const prevVal = Number(c.data[k].split("/")[0])
                                    const prevHit = Number(c.data[k].split("/")[1])
                                    const prevCount = Number(c.data[k].split("/")[2])

                                    let res = `${newValueAttribute}/${prevHit + 1}/${prevVal <= valueAttribute ? prevCount + 1 : prevCount
                                        }`
                                    if (Number(k.slice(-2)) === prevHit) {
                                        tempData[k] = `${newValueAttribute}/1/1`
                                    } else {
                                        tempData[k] = res
                                    }
                                }
                            }
                        })

                        // DATE
                        // ---------------------------------------
                        function checkDatetime(loc) {
                            let f = new Intl.DateTimeFormat("en", {
                                year: "numeric",
                                month: "2-digit",
                                day: "2-digit",
                                weekday: "short",
                                hour: "2-digit",
                                hour12: false,
                                minute: "2-digit",
                                second: "2-digit",
                                timeZone: "Asia/Jakarta",
                            })
                            let temp = f.formatToParts(new Date())
                            let parts = temp.reduce((acc, part) => {
                                if (part.type != "literal") {
                                    acc[part.type] = part.value
                                }
                                return acc
                            }, Object.create(null))
                            return `${parts.day}-${parts.month}-${parts.year}/${parts.hour}:${parts.minute}:${parts.second}`
                        }

                        tempData.date = checkDatetime()

                        new_data_coins.push(tempData);
                        // UPDATE COINS
                        // const updateCoins = await axios.put(
                        //     "https://api.tambahaset.com/coins/v1/updateCoins",
                        //     tempData
                        // )
                    })
                })

                // Switch Error
                // ---------------------------------------
                .catch((error) => {
                    if (error.hasOwnProperty("response")) {
                        let responseError = error.response;

                        if (responseError.hasOwnProperty("data")) {
                            // errorResponse = responseError.data;
                            res.send({ status: 403, data: responseError.data });
                        }
                    }
                    // console.error(error.response.data)
                })

            res.send({ status: 200, data: new_data_coins });
        }


    },

    /*
    |------------------------------------------------------------------------
    | Get Coins Function
    |------------------------------------------------------------------------
    */
    async sync_coin_now(req, res) {
        const taapi_secret = process.env.TAAPI_SECRET || "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbHVlIjoiNjNmNWRjYTJlYzkzMzI0NGEwZjI4MGQ5IiwiaWF0IjoxNjgyNzg3NjY0LCJleHAiOjMzMTg3MjUxNjY0fQ.FQ5FBzctPkBLwcCjiyrpvBJY9HY8OdJHJmeekp4NjF4"
        const max_request_bulk = 10
        const taapi = new Taapi.default(taapi_secret);

        // Init bulk queries. This resets all previously added queries
        // ---------------------------------------
        taapi.resetBulkConstructs();


        // Get Data Symbol
        // ---------------------------------------
        const data_symbol = await SymbolModel.find({});

        // Populate data bulkdata
        // ---------------------------------------
        const bulk_data = [];
        const exchange = process.env.TAAPI_EXCHANGE || "binance"

        const indicator = ["ma", "ema"]
        if (Array.isArray(data_symbol) && data_symbol.length > 0) {
            data_symbol.map(async (x, idx) => {
                if (idx < max_request_bulk) {
                    const item = {}
                    item.interval = "5m"
                    item.exchange = exchange
                    item.coin = x.coin

                    indicator.map(async (idc, _idx) => {
                        item.indicator = idc
                    })

                    bulk_data.push(item)
                }
            })
        }

        let limit_data_request = 10
        let error = '';

        if (Array.isArray(bulk_data) && bulk_data.length > 0) {
            bulk_data.map(async (item, idx) => {
                if (limit_data_request >= (idx + 1)) {
                    indicator.map((idc, indexIndicator) => {
                        if ((indexIndicator + 1) >= 2) {
                            taapi.addCalculation(idc, item.coin, item.interval, `${item.coin}`, { period: 10, backtrack: 10 });
                        }
                    })

                }
            })

        }

        const response_data = []

        await taapi.executeBulk().then(results => {
            response_data.push(results);

        }).catch(error => {
            console.error(error.response)
        });

        res.send({ status: 200, data: response_data, message: "Data Coin Berhasil diambil!" });
    },

    /*
    |------------------------------------------------------------------------
    | Generate Data Function
    |------------------------------------------------------------------------
    */
    async generate_coin(req, res) {
        const taapi_secret = process.env.TAAPI_SECRET || "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbHVlIjoiNjNmNWRjYTJlYzkzMzI0NGEwZjI4MGQ5IiwiaWF0IjoxNjgyNzg3NjY0LCJleHAiOjMzMTg3MjUxNjY0fQ.FQ5FBzctPkBLwcCjiyrpvBJY9HY8OdJHJmeekp4NjF4"
        const taapi = new Taapi.default(taapi_secret);


        taapi.getIndicator("rsi", "BTC/USDT", "5m").then(rsi => {
            res.send({ status: 200, data: rsi });
        });
    },
}
