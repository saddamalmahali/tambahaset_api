const OptionModel = require("../model/Options")

module.exports = {

    /*
    |------------------------------------------------------------------------
    |  Create Option Vairable Functions
    |------------------------------------------------------------------------
    */
    async create_options(req, res) {


        const name = req.body.name || '';
        const value = req.body.value || '';

        if (!name || !value) {
            res.send({ status: 403, message: "Data Harus Komplit" })
        }

        const data_res = OptionModel.create({ name, value });

        res.send({ status: 201, message: "Berhasil Membuat Data!", data: data_res })

    },


    /*
    |------------------------------------------------------------------------
    |  Enable Short Term Cron Functions
    |------------------------------------------------------------------------
    */
    async cron_short_term_enable(req, res) {

        const data_cron = await OptionModel.findOne({ name: 'cron_short_term_active' }).exec();


        if (data_cron) {
            const doc = await OptionModel.findOneAndUpdate({
                name: 'cron_short_term_active'
            }, { value: true }, { upsert: true, useFindAndModify: false });
        }

        res.send({ status: 201, message: "Berhasil Mengaktifkan Cron!" })
    },

    /*
    |------------------------------------------------------------------------
    |  Disable Short Term Cron Functions
    |------------------------------------------------------------------------
    */
    async cron_short_term_disable(req, res) {
        const data_cron = await OptionModel.findOne({ name: 'cron_short_term_active' }).exec();


        if (data_cron) {
            const doc = await OptionModel.findOneAndUpdate({
                name: 'cron_short_term_active'
            }, { value: false }, { upsert: true, useFindAndModify: false });
        }

        res.send({ status: 201, message: "Berhasil Menonaktifkan Cron!" })
    },

}