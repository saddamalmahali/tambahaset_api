const express = require("express")
const router = express.Router()

// =======================================
// Import Middleware
// =======================================
const authMiddleware = require('../middleware/auth')

const { create_options, cron_short_term_enable, cron_short_term_disable } = require("../controllers/options")

router.post("/options/create", create_options)
router.post("/options/cron_short_term/enable", cron_short_term_enable)
router.post("/options/cron_short_term/disable", cron_short_term_disable)


module.exports = router