const auth = require("./auth")
const coins = require("./coins")
const options = require("./option")

module.exports = { auth, coins, options };