const express = require("express")
const router = express.Router()

// =======================================
// Import Middleware
// =======================================
const authMiddleware = require('../middleware/auth')

const { get_all_coins, sync_coin, generate_coin, sync_coin_now } = require("../controllers/coins")

router.get("/coins/get_all_coins", get_all_coins)
router.post("/coins/sync_coin", sync_coin)
router.post("/coins/sync_coin_now", sync_coin_now)
router.get("/coins/generate_coin", generate_coin)


module.exports = router