const express = require('express')
const bodyParser = require('body-parser')
const coin_symbol = require('./data/symbol')
const app = express()
const port = process.env.PORT || 8000
const limiter = require("./config/limit")
const cron = require('node-cron');

// =======================================
// Connect To Database
// =======================================
require("./config/database").connect();

// =======================================
// Import Model
// =======================================
const Coin = require('./model/Coin')
const Symbol = require('./model/Symbol')

// =======================================
// Import Controller
// =======================================
const routerAuth = require("./router").auth
const routerCoins = require("./router").coins
const routerOptions = require("./router").options

// =======================================
// Import Middleware
// =======================================
const authMiddleware = require('./middleware/auth')

// =======================================
// FEATURE CONFIG
// =======================================
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
// app.use(limiter)
app.use(function (req, res, next) {
    // Allow all domains to make requests
    res.header("Access-Control-Allow-Origin", "*")
    // Allow specific HTTP methods
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE")
    // Allow specific headers
    res.header("Access-Control-Allow-Headers", "Content-Type")
    // Allow credentials to be included in requests
    res.header("Access-Control-Allow-Credentials", true)
    next()
})

// =======================================
// Config Cron
// =======================================
// require('./cron/shortTerm').cronShortTerm();



cron.schedule('1 * * * * *', () => {
    require('./cron/shortTerm').cronShortTerm();
});

// =======================================
// Config Router
// =======================================
app.use("/v1/", routerAuth)
app.use("/v1/", routerCoins)
app.use("/v1/", routerOptions)


app.listen(port, () => console.log(`Berjalan di port ${port}`))