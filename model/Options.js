const mongoose = require('mongoose');

let { Schema } = mongoose

const optionsSchema = new Schema({
    name: {
        type: Schema.Types.String,
        required: true
    },
    value: {
        type: Schema.Types.Boolean,
        required: true
    }
});

module.exports = mongoose.model('Options', optionsSchema);