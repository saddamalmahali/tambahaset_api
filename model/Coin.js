const mongoose = require('mongoose');

let { Schema } = mongoose

const coinSchema = new Schema({
    symbol: {
        type: Schema.Types.String,
        required: true
    },
    exchange: {
        type: Schema.Types.String,
        required: true
    },
    value: {
        type: Schema.Types.Number,
        required: true
    },
    interval: {
        type: Schema.Types.String,
        required: true
    },
    indicator: {
        type: Schema.Types.String,
        required: true
    },
    status: {
        type: Schema.Types.String,
        required: true
    },
    date_created: {
        type: Schema.Types.Date,
        required: true
    }
});

module.exports = mongoose.model('Coin', coinSchema);