const mongoose = require('mongoose');

let { Schema } = mongoose

const symbolSchema = new Schema({
    coin: {
        type: Schema.Types.String,
        required: true
    },
    createdAt: {
        type: Schema.Types.Date,
        required: true
    },
    updatedAt: {
        type: Schema.Types.Date,
        required: true
    },
});

// compile our model
const Symbol = mongoose.model('Symbol', symbolSchema);

module.exports = Symbol