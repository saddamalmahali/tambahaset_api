const OptionModel = require('../model/Options');
const SymbolModel = require('../model/Symbol');
const CoinModel = require('../model/Coin')
const Taapi = require("taapi");
const indicator = ["ma", "ema", "dema", "kama", "macd", "mama", "hma"]
const _ = require('lodash')
const moment = require('moment')

// Chunk Data Indicator
// ---------------------------------------
const chunkIndicator = _.chunk(indicator, 7);

const getDataShortTerm = async () => {
    const cron_short_term_active = await OptionModel.findOne({ name: 'cron_short_term_active' }).exec();

    if (cron_short_term_active) {
        if (cron_short_term_active.value == true) {
            const taapi_secret = process.env.TAAPI_SECRET || "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbHVlIjoiNjNmNWRjYTJlYzkzMzI0NGEwZjI4MGQ5IiwiaWF0IjoxNjgyNzg3NjY0LCJleHAiOjMzMTg3MjUxNjY0fQ.FQ5FBzctPkBLwcCjiyrpvBJY9HY8OdJHJmeekp4NjF4"
            const taapi = new Taapi.default(taapi_secret);

            // Logic if Cron Enabled
            // ---------------------------------------
            const datecreated = moment();

            // 1. Get Coin
            // ---------------------------------------
            const data_coin = await SymbolModel.find({}).limit(50);

            if (Array.isArray(data_coin) && data_coin.length > 0) {

                // Chunk Data Coin
                // ---------------------------------------
                const chunk = _.chunk(data_coin, 10);

                if (Array.isArray(chunk) && chunk.length > 0) {


                    // =======================================
                    // Forloop Chunk Coin
                    // =======================================
                    for (let iChunk = 0; iChunk < chunk.length; iChunk++) {

                        let chunk_data = await chunk[iChunk];
                        // Init bulk queries. This resets all previously added queries
                        // ---------------------------------------


                        if (Array.isArray(chunk_data) && chunk_data.length > 0) {

                            // =======================================
                            // Forloop Chunk Indicator
                            // =======================================
                            for (let iin = 0; iin < chunkIndicator.length; iin++) {
                                console.log(`Chung [${iChunk}][${iin}] : `);

                                for (let cdi = 0; cdi < chunk_data.length; cdi++) {
                                    taapi.resetBulkConstructs();

                                    let coin_symbol = chunk_data[cdi]

                                    for (let ii = 0; ii < chunkIndicator[iin].length; ii++) {
                                        taapi.addCalculation(chunkIndicator[iin][ii], coin_symbol.coin, "5m", `${coin_symbol.coin}`, { period: 10 });
                                    }

                                    const response_data = []


                                    await taapi.executeBulk().then(async (results) => {
                                        response_data.push(results);

                                        const insert_data = []

                                        // Save To Database
                                        // ---------------------------------------
                                        await chunk_data.map(async (coin_symbol, indexChunkItem) => {
                                            if (results[coin_symbol.coin]) {

                                                chunkIndicator[iin].map(async (idc, _idx) => {

                                                    const coinData = results[coin_symbol.coin];
                                                    let value_data = 0;

                                                    if (Array.isArray(coinData) && coinData.length > 0) {
                                                        value_data = coinData[_idx].value || 0
                                                    } else {
                                                        value_data = coinData.value || 0
                                                    }

                                                    // If Value MACD Available
                                                    // ---------------------------------------
                                                    if (coinData.hasOwnProperty('valueMACD')) {
                                                        value_data = coinData.valueMACD
                                                    }

                                                    // If valueMAMA Avalilable
                                                    // ---------------------------------------
                                                    if (coinData.hasOwnProperty('valueMAMA')) {
                                                        value_data = coinData.valueMAMA
                                                    }

                                                    // CoinModel.create({
                                                    //     symbol: coin_symbol.coin,
                                                    //     exchange: 'binance',
                                                    //     value: value_data,
                                                    //     indicator: idc,
                                                    //     interval: '5m',
                                                    //     date_created: datecreated,
                                                    //     status: 0
                                                    // })

                                                    insert_data.push(({
                                                        symbol: coin_symbol.coin,
                                                        exchange: 'binance',
                                                        value: value_data,
                                                        indicator: idc,
                                                        interval: '5m',
                                                        date_created: datecreated,
                                                        status: 0
                                                    }));
                                                });
                                            }
                                        })

                                        if (Array.isArray(insert_data) && insert_data.length > 0) {
                                            CoinModel.insertMany(insert_data);
                                        }

                                        console.log('Data Response : ', JSON.stringify(insert_data))

                                    }).catch(error => {
                                        let response = error.response;

                                        if (response.hasOwnProperty("data")) {
                                            console.error(response.data)
                                        }

                                    });

                                }



                            }

                        }
                    }

                }
            }

        }


        // 2. in each coin, get 
        // ---------------------------------------


    } else {
        // Cron Disabled
        // ---------------------------------------
    }
}

const chunkData = (data) => {
    const chunkSize = 10;
    const chunk_data = [];
    for (let i = 0; i < data.length; i += chunkSize) {
        const chunk = data.slice(i, i + chunkSize);
        chunk_data.push(chunk);
    }

    return chunk_data;
}

exports.cronShortTerm = getDataShortTerm